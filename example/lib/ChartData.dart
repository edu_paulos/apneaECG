import 'package:flutter/material.dart';

class ChartData extends ChangeNotifier {
  final int timestamp;
  final int sample;
  ChartData(this.timestamp, this.sample);
}
