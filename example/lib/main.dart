import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:apneaecg/AppModel.dart';
import 'package:provider/provider.dart';

import 'ScanWidget.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  EasyLoading.init();
  await Firebase.initializeApp();

  runApp(ChangeNotifierProvider(
    create: (context) => AppModel(),
    child: MaterialApp(
      home: ScanWidget(),
    ),
  ));
}
