import 'dart:convert';
import 'dart:ffi';
import 'dart:math';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:mdsflutter/Mds.dart';
import 'package:csv/csv.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';

import 'dart:developer' as developer;

import 'package:provider/provider.dart';
import 'FirebaseApi.dart';

import 'ChartData.dart';

class DeviceModel extends ChangeNotifier {
  final int MS_IN_SECOND = 1000;
  final int ecgSampleRate = 125;

  UploadTask _task;
  UploadTask get task => _task;

  List<ChartData> _chartData = <ChartData>[];
  List<ChartData> get chartData => _chartData;

  String _serial;
  String _name;

  String get name => _name;
  String get serial => _serial;

  int _accSubscription;
  String _accelerometerData = "";
  String get accelerometerData => _accelerometerData;
  bool get accelerometerSubscribed => _accSubscription != null;

  int _ecgSubscription;
  String _ecgData = "";
  List<List<dynamic>> ecgArray = [];
  String get ecgData => _ecgData;
  bool get ecgSubscribed => _ecgSubscription != null;

  int _hrSubscription;
  String _hrData = "";
  String get hrData => _hrData;
  bool get hrSubscribed => _hrSubscription != null;

  bool _ledStatus = false;
  bool get ledStatus => _ledStatus;

  String _temperature = "";
  String get temperature => _temperature;

  DeviceModel(this._name, this._serial);

  void subscribeToAccelerometer() {
    _accelerometerData = "";
    _accSubscription = Mds.subscribe(
        Mds.createSubscriptionUri(_serial, "/Meas/Acc/250"),
        "{}",
        (d, c) => {},
        (e, c) => {},
        (data) => _onNewAccelerometerData(data),
        (e, c) => {});
    notifyListeners();
  }

  void _onNewAccelerometerData(String data) {
    Map<String, dynamic> accData = jsonDecode(data);
    Map<String, dynamic> body = accData["Body"];
    List<dynamic> accArray = body["ArrayAcc"];
    dynamic acc = accArray.last;
    _accelerometerData = "x: " +
        acc["x"].toStringAsFixed(2) +
        "\ny: " +
        acc["y"].toStringAsFixed(2) +
        "\nz: " +
        acc["z"].toStringAsFixed(2);
    notifyListeners();
  }

  void unsubscribeFromAccelerometer() {
    Mds.unsubscribe(_accSubscription);
    _accSubscription = null;
    notifyListeners();
  }

  void subscribeToECG() {
    _ecgData = "";
    ecgArray = [];
    //_chartData = [];
    _ecgSubscription = Mds.subscribe(
        Mds.createSubscriptionUri(_serial, "/Meas/ECG/125"),
        "{}",
        (d, c) => {},
        (e, c) => {},
        (data) => _onNewECGData(data),
        (e, c) => {});
    notifyListeners();
  }

  void _onNewECGData(String data) {
    final double sampleInterval = MS_IN_SECOND / ecgSampleRate;

    Map<String, dynamic> ecgData = jsonDecode(data);
    Map<String, dynamic> body = ecgData["Body"];
    var timestamp = body["Timestamp"];
    List<dynamic> ecgSample = body["Samples"];

    for (int i = 0; i < ecgSample.length; i++) {
      ecgArray.add([timestamp + (sampleInterval * i).round(), ecgSample[i]]);
      _chartData.add(
          ChartData(timestamp + (sampleInterval * i).round(), ecgSample[i]));
    }

    if (_chartData.length > 200) {
      chartData.removeRange(0, 16);
    }

    notifyListeners();
  }

  void subscribeToHr() {
    _hrData = "";
    _hrSubscription = Mds.subscribe(
        Mds.createSubscriptionUri(_serial, "/Meas/HR"),
        "{}",
        (d, c) => {},
        (e, c) => {},
        (data) => _onNewHrData(data),
        (e, c) => {});
    notifyListeners();
  }

  void _onNewHrData(String data) {
    Map<String, dynamic> hrData = jsonDecode(data);
    Map<String, dynamic> body = hrData["Body"];
    double hr = body["average"];
    _hrData = hr.toStringAsFixed(1) + " bpm";
    notifyListeners();
  }

  void unsubscribeFromHr() {
    Mds.unsubscribe(_hrSubscription);
    _hrSubscription = null;
    notifyListeners();
  }

  Future unsubscribeFromECG() async {
    Mds.unsubscribe(_ecgSubscription);
    _ecgSubscription = null;

    notifyListeners();
  }

  void switchLed() {
    Map<String, bool> contract = new Map<String, bool>();
    contract["isOn"] = !_ledStatus;
    Mds.put(
        Mds.createRequestUri(_serial, "/Component/Led"), jsonEncode(contract),
        (data, code) {
      _ledStatus = !_ledStatus;
      notifyListeners();
    }, (e, c) => {});
  }

/*
  void getTemperature() {
    Mds.get(Mds.createRequestUri(_serial, "/Meas/Temp"), "{}", (data, code) {
      double kelvin = jsonDecode(data)["Content"]["Measurement"];
      double temperatureVal = kelvin - 274.15;
      _temperature = temperatureVal.toStringAsFixed(1) + " C";
      notifyListeners();
    }, (e, c) => {});
  }
*/
  void getECGInfo() {
    Mds.get(Mds.createRequestUri(_serial, "/Meas/ECG/Info"), "{}",
        (data, code) {
      double kelvin = jsonDecode(data)["content"]["CurrentSampleRate"];
      double temperatureVal = kelvin;
      _temperature = temperatureVal.toStringAsFixed(1);
      notifyListeners();
    }, (e, c) => {});
  }
}
