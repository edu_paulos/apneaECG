import 'dart:async';
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:apneaecg/DeviceModel.dart';
import 'package:provider/provider.dart';

import 'package:apneaecg/Device.dart';

import 'AppModel.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:csv/csv.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

import 'FirebaseApi.dart';
import 'ChartData.dart';

class DeviceInteractionWidget extends StatefulWidget {
  final Device device;
  const DeviceInteractionWidget(this.device);

  @override
  State<StatefulWidget> createState() {
    return _DeviceInteractionWidgetState();
  }
}

class _DeviceInteractionWidgetState extends State<DeviceInteractionWidget> {
  AppModel _appModel;
  UploadTask task;

  ChartSeriesController _chartSeriesController;
  int count = 10;

  @override
  void initState() {
    super.initState();
    _appModel = Provider.of<AppModel>(context, listen: false);
    _appModel.onDeviceMdsDisconnected((device) => {Navigator.pop(context)});
  }

  void _onAccelerometerButtonPressed(DeviceModel deviceModel) {
    if (deviceModel.accelerometerSubscribed) {
      deviceModel.unsubscribeFromAccelerometer();
    } else {
      deviceModel.subscribeToAccelerometer();
    }
  }

  void _onECGButtonPressed(DeviceModel deviceModel) async {
    if (deviceModel.ecgSubscribed) {
      print(deviceModel.chartData);
      print("TEST--------------->");
      String csv = const ListToCsvConverter().convert(deviceModel.ecgArray);
      print(csv);
      final directory = await getApplicationDocumentsDirectory();
      final pathOfTheFileToWrite = directory.path + "/myCsvFile.csv";
      print(pathOfTheFileToWrite);
      File file = await File(pathOfTheFileToWrite);
      file.writeAsString(csv);
      var now = DateTime.now();
      final destination = '$now.csv';
      task = FirebaseApi.uploadFile(destination, file);
      task != null ? buildUploadStatus(task) : Container();

      deviceModel.unsubscribeFromECG();
    } else {
      deviceModel.subscribeToECG();
    }
  }

  void _onHrButtonPressed(DeviceModel deviceModel) {
    if (deviceModel.hrSubscribed) {
      deviceModel.unsubscribeFromHr();
    } else {
      deviceModel.subscribeToHr();
    }
  }

  @override
  void dispose() {
    _appModel.disconnectFromDevice(widget.device);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Device device = widget.device;

    return ChangeNotifierProvider(
      create: (context) => DeviceModel(device.name, device.serial),
      child: Consumer<DeviceModel>(
        builder: (context, model, child) {
          return Scaffold(
              appBar: AppBar(
                title: Text(device.name),
              ),
              body: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _ecgItem(model),
                  _accelerometerItem(model),
                  _hrItem(model),
                  _ledItem(model),
                  _temperatureItem(model),
                  _buildLiveChart(model),
                ],
              ));
        },
      ),
    );
  }

  Widget _ecgItem(DeviceModel deviceModel) {
    return Card(
      child: ListTile(
        title: Text("ECG"),
        subtitle: Text(""),
        trailing: RaisedButton(
          child: Text(deviceModel.ecgSubscribed ? "Unsubscribe" : "Subscribe"),
          onPressed: () => _onECGButtonPressed(deviceModel),
        ),
      ),
    );
  }

  Widget _accelerometerItem(DeviceModel deviceModel) {
    return Card(
      child: ListTile(
        title: Text("Accelerometer"),
        subtitle: Text(deviceModel.accelerometerData),
        trailing: RaisedButton(
          child: Text(deviceModel.accelerometerSubscribed
              ? "Unsubscribe"
              : "Subscribe"),
          onPressed: () => _onAccelerometerButtonPressed(deviceModel),
        ),
      ),
    );
  }

  Widget _hrItem(DeviceModel deviceModel) {
    return Card(
      child: ListTile(
        title: Text("Heart rate"),
        subtitle: Text(deviceModel.hrData),
        trailing: RaisedButton(
          child: Text(deviceModel.hrSubscribed ? "Unsubscribe" : "Subscribe"),
          onPressed: () => _onHrButtonPressed(deviceModel),
        ),
      ),
    );
  }

  Widget _ledItem(DeviceModel deviceModel) {
    return Card(
      child: ListTile(
        title: Text("Led"),
        trailing: Switch(
          value: deviceModel.ledStatus,
          onChanged: (b) => {deviceModel.switchLed()},
        ),
      ),
    );
  }

  Widget _temperatureItem(DeviceModel deviceModel) {
    return Card(
      child: ListTile(
        title: Text("Temperature"),
        subtitle: Text(deviceModel.temperature),
        trailing: RaisedButton(
          child: Text("Get"),
          onPressed: () => deviceModel.getECGInfo(),
        ),
      ),
    );
  }

  Widget buildUploadStatus(UploadTask task) => StreamBuilder(
      stream: task.snapshotEvents,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final snap = snapshot.data;
          final progress = snap.bytesTransferred / snap.totalBytes;
          final percentage = (progress * 100).toStringAsFixed(2);

          return Center(
            child: LinearProgressIndicator(value: progress),
          );
        } else {
          return Container();
        }
      });

  SfCartesianChart _buildLiveChart(DeviceModel deviceModel) {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryXAxis: NumericAxis(majorGridLines: MajorGridLines(width: 0)),
      primaryYAxis: NumericAxis(
          axisLine: AxisLine(width: 0),
          majorTickLines: MajorTickLines(size: 0)),
      series: <LineSeries<ChartData, int >>[
        LineSeries<ChartData, int>(
          onRendererCreated: (ChartSeriesController controller) {
            _chartSeriesController = controller;
          },
          dataSource: deviceModel.chartData,
          color: const Color.fromRGBO(192, 108, 132, 1),
          xValueMapper: (ChartData ecg, _) => ecg.timestamp,
          yValueMapper: (ChartData ecg, _) => ecg.sample,
          animationDuration: 0,
        )
      ],
    );
  }

}
